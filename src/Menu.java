import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu {
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JButton checkOutButton;
    private JTextPane textPane1;
    private JPanel root;
    private JLabel Total;
    private String menu="";
    private int cost=0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Menu");
        frame.setContentPane(new Menu().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    void order(String food){
        int result = JOptionPane.showConfirmDialog(
                null,
                "Would you like to "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(result == 0){
            menu += food+"\n";
            cost += 100;
            JOptionPane.showMessageDialog(null, "Thank you for ordering "+food+"! It will be served as soon as possible.");
            textPane1.setText(menu);
            Total.setText("Total   "+cost+" yen");
        }
    };


    public Menu() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(result == 0){
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+cost+" yen.");
                    cost = 0;
                    menu = "";
                    textPane1.setText(menu);
                    Total.setText("Total   "+cost+" yen");
                }
            }
        });
    }
}
